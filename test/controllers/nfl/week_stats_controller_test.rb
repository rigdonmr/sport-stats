require 'test_helper'

class Nfl::WeekStatsControllerTest < ActionDispatch::IntegrationTest
  test "should get show" do
    get nfl_week_stat_url(week: 1)
    assert_response :success
  end

end

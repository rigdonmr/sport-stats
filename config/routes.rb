# == Route Map
#
#        Prefix Verb URI Pattern                     Controller#Action
# nfl_week_stat GET  /nfl/week_stats/:week(.:format) nfl/week_stats#show
#

Rails.application.routes.draw do
  namespace :nfl do
    resources :week_stats,
              param: :week,
              defaults: { format: :json },
              only: [:show]
  end
end

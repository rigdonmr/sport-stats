class CreateNflKickingStats < ActiveRecord::Migration[5.1]
  def change
    create_table :nfl_kicking_stats do |t|
      t.string :entry_id
      t.references :nfl_week_stat, foreign_key: true
      t.integer :fld_goals_made
      t.integer :fld_goals_att
      t.integer :extra_pt_made
      t.integer :extra_pt_att

      t.timestamps
    end
  end
end

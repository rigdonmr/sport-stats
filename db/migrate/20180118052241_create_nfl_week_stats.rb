class CreateNflWeekStats < ActiveRecord::Migration[5.1]
  def change
    create_table :nfl_week_stats do |t|
      t.integer :week, index: true
      t.references :nfl_player, foreign_key: true

      t.timestamps
    end
  end
end

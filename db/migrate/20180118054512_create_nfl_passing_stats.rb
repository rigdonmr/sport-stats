class CreateNflPassingStats < ActiveRecord::Migration[5.1]
  def change
    create_table :nfl_passing_stats do |t|
      t.string :entry_id
      t.references :nfl_week_stat, foreign_key: true
      t.integer :yds
      t.integer :tds
      t.integer :att
      t.integer :cmp
      t.integer :int

      t.timestamps
    end
  end
end

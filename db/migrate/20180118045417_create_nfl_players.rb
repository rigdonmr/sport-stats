class CreateNflPlayers < ActiveRecord::Migration[5.1]
  def change
    create_table :nfl_players do |t|
      t.string :player_id
      t.string :position
      t.string :name

      t.timestamps
    end
  end
end

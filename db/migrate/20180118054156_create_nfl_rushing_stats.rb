class CreateNflRushingStats < ActiveRecord::Migration[5.1]
  def change
    create_table :nfl_rushing_stats do |t|
      t.string :entry_id
      t.references :nfl_week_stat, foreign_key: true
      t.integer :yds
      t.integer :tds
      t.integer :att
      t.integer :fum

      t.timestamps
    end
  end
end

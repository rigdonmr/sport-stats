# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20180118054815) do

  create_table "nfl_kicking_stats", force: :cascade do |t|
    t.string "entry_id"
    t.integer "nfl_week_stat_id"
    t.integer "fld_goals_made"
    t.integer "fld_goals_att"
    t.integer "extra_pt_made"
    t.integer "extra_pt_att"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["nfl_week_stat_id"], name: "index_nfl_kicking_stats_on_nfl_week_stat_id"
  end

  create_table "nfl_passing_stats", force: :cascade do |t|
    t.string "entry_id"
    t.integer "nfl_week_stat_id"
    t.integer "yds"
    t.integer "tds"
    t.integer "att"
    t.integer "cmp"
    t.integer "int"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["nfl_week_stat_id"], name: "index_nfl_passing_stats_on_nfl_week_stat_id"
  end

  create_table "nfl_players", force: :cascade do |t|
    t.string "player_id"
    t.string "position"
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "nfl_receiving_stats", force: :cascade do |t|
    t.string "entry_id"
    t.integer "nfl_week_stat_id"
    t.integer "yds"
    t.integer "tds"
    t.integer "rec"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["nfl_week_stat_id"], name: "index_nfl_receiving_stats_on_nfl_week_stat_id"
  end

  create_table "nfl_rushing_stats", force: :cascade do |t|
    t.string "entry_id"
    t.integer "nfl_week_stat_id"
    t.integer "yds"
    t.integer "tds"
    t.integer "att"
    t.integer "fum"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["nfl_week_stat_id"], name: "index_nfl_rushing_stats_on_nfl_week_stat_id"
  end

  create_table "nfl_week_stats", force: :cascade do |t|
    t.integer "week"
    t.integer "nfl_player_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["nfl_player_id"], name: "index_nfl_week_stats_on_nfl_player_id"
    t.index ["week"], name: "index_nfl_week_stats_on_week"
  end

end

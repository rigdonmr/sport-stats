file = File.read 'sample-input.json'
data = JSON.parse file

week = data['week'].to_i

data['kicking'].each do |kicking|
  player = Nfl::Player.find_or_create_by! player_id: kicking['player_id'],
                                          name: kicking['name'],
                                          position: kicking['position']

  week_stat = player.week_stats.find_or_create_by! week: week

  week_stat.create_kicking_stat! fld_goals_made: kicking['fld_goals_made'],
                                 fld_goals_att: kicking['fld_goals_att'],
                                 extra_pt_att: kicking['extra_pt_att'],
                                 extra_pt_made: kicking['extra_pt_made']
end

data['passing'].each do |passing|
  player = Nfl::Player.find_or_create_by! player_id: passing['player_id'],
                                          name: passing['name'],
                                          position: passing['position']

  week_stat = player.week_stats.find_or_create_by! week: week

  week_stat.create_passing_stat! yds: passing['yds'],
                                 att: passing['att'],
                                 tds: passing['tds'],
                                 cmp: passing['cmp']
end

data['receiving'].each do |receiving|
  player = Nfl::Player.find_or_create_by! player_id: receiving['player_id'],
                                          name: receiving['name'],
                                          position: receiving['position']

  week_stat = player.week_stats.find_or_create_by! week: week

  week_stat.create_receiving_stat! yds: receiving['yds'],
                                   tds: receiving['tds'],
                                   rec: receiving['rec']
end

data['rushing'].each do |rushing|
  player = Nfl::Player.find_or_create_by! player_id: rushing['player_id'],
                                          name: rushing['name'],
                                          position: rushing['position']

  week_stat = player.week_stats.find_or_create_by! week: week

  week_stat.create_rushing_stat! yds: rushing['yds'],
                                 att: rushing['att'],
                                 tds: rushing['tds'],
                                 fum: rushing['fum']
end

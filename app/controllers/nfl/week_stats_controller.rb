class Nfl::WeekStatsController < ApplicationController
  def show
    @week_stats = Nfl::WeekStat
                  .where(week: params[:week])
                  .includes(%i[
                    player
                    receiving_stat
                    rushing_stat
                    passing_stat
                    kicking_stat
                  ])

    narrow_by_players?
  end

  private

  def narrow_by_players?
    players = params[:players]

    if valid_players_format?(players)
      @week_stats = @week_stats
                    .where(player: players.split(','))

      return true
    end

    false
  end

  def valid_players_format?(players)
    players =~ /^(\d+(,\d+)*)?$/
  end
end

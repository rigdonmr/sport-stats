@week_stats.each do |week_stat|
  player = week_stat.player

  json.receiving do
    receiving_stat = week_stat.receiving_stat

    if receiving_stat
      json.child! do
        json.player_id player.player_id
        json.entry_id receiving_stat.entry_id
        json.name player.name
        json.position player.position
        json.yds receiving_stat.yds
        json.tds receiving_stat.tds
        json.rec receiving_stat.rec
      end
    end
  end

  json.rushing do
    rushing_stat = week_stat.rushing_stat

    if rushing_stat
      json.child! do
        json.player_id player.player_id
        json.entry_id rushing_stat.entry_id
        json.name player.name
        json.position player.position
        json.yds rushing_stat.yds
        json.att rushing_stat.att
        json.tds rushing_stat.tds
        json.fum rushing_stat.fum
      end
    end
  end

  json.passing do
    passing_stat = week_stat.passing_stat

    if passing_stat
      json.child! do
        json.player_id player.player_id
        json.entry_id passing_stat.entry_id
        json.name player.name
        json.position player.position
        json.yds passing_stat.yds
        json.att passing_stat.att
        json.tds passing_stat.tds
        json.cmp passing_stat.cmp
        json.int passing_stat.int
      end
    end
  end

  json.kicking do
    kicking_stat = week_stat.kicking_stat

    if kicking_stat
      json.child! do
        json.player_id player.player_id
        json.entry_id kicking_stat.entry_id
        json.name player.name
        json.position player.position
        json.fld_goals_made kicking_stat.fld_goals_made
        json.fld_goals_att kicking_stat.fld_goals_att
        json.extra_pt_made kicking_stat.extra_pt_made
        json.extra_pt_att kicking_stat.extra_pt_att
      end
    end
  end
end

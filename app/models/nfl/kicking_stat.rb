# == Schema Information
#
# Table name: nfl_kicking_stats
#
#  id               :integer          not null, primary key
#  entry_id         :string
#  nfl_week_stat_id :integer
#  fld_goals_made   :integer
#  fld_goals_att    :integer
#  extra_pt_made    :integer
#  extra_pt_att     :integer
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#

class Nfl::KickingStat < ApplicationRecord
  belongs_to :week_stat,
             class_name: 'Nfl::WeekStat',
             foreign_key: :nfl_week_stat_id,
             inverse_of: :kicking_stat
end

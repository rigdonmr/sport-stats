# == Schema Information
#
# Table name: nfl_passing_stats
#
#  id               :integer          not null, primary key
#  entry_id         :string
#  nfl_week_stat_id :integer
#  yds              :integer
#  tds              :integer
#  att              :integer
#  cmp              :integer
#  int              :integer
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#

class Nfl::PassingStat < ApplicationRecord
  belongs_to :week_stat,
             class_name: 'Nfl::WeekStat',
             foreign_key: :nfl_week_stat_id,
             inverse_of: :passing_stat
end

# == Schema Information
#
# Table name: nfl_week_stats
#
#  id            :integer          not null, primary key
#  week          :integer
#  nfl_player_id :integer
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#

class Nfl::WeekStat < ApplicationRecord
  belongs_to :player,
             class_name: 'Nfl::Player',
             foreign_key: :nfl_player_id,
             inverse_of: :week_stats

  has_one :receiving_stat,
          class_name: 'Nfl::ReceivingStat',
          foreign_key: 'nfl_week_stat_id',
          inverse_of: :week_stat

  has_one :rushing_stat,
          class_name: 'Nfl::RushingStat',
          foreign_key: 'nfl_week_stat_id',
          inverse_of: :week_stat

  has_one :passing_stat,
          class_name: 'Nfl::PassingStat',
          foreign_key: 'nfl_week_stat_id',
          inverse_of: :week_stat

  has_one :kicking_stat,
          class_name: 'Nfl::KickingStat',
          foreign_key: 'nfl_week_stat_id',
          inverse_of: :week_stat
end

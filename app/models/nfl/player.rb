# == Schema Information
#
# Table name: nfl_players
#
#  id         :integer          not null, primary key
#  player_id  :string
#  position   :string
#  name       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Nfl::Player < ApplicationRecord
  has_many :week_stats,
           class_name: 'Nfl::WeekStat',
           foreign_key: 'nfl_player_id',
           inverse_of: :player
end

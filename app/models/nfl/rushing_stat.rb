# == Schema Information
#
# Table name: nfl_rushing_stats
#
#  id               :integer          not null, primary key
#  entry_id         :string
#  nfl_week_stat_id :integer
#  yds              :integer
#  tds              :integer
#  att              :integer
#  fum              :integer
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#

class Nfl::RushingStat < ApplicationRecord
  belongs_to :week_stat,
             class_name: 'Nfl::WeekStat',
             foreign_key: :nfl_week_stat_id,
             inverse_of: :rushing_stat
end

# Installation

0. Have `ruby 2.5.0`, `sqlite3`, `rails`, and `bundler` installed
1. `git clone https://gitlab.com/rigdonmr/sport-stats`
2. `cd sport-stats`
3. `rails db:migrate`
4. `rails db:seed`
5. `rails s`
6. Navigate to `http://localhost:3000/nfl/week_stats/4` to see a sample json with the seed data
7. Add `?players=1,2,3` to see the filter in use. The params correspond to the db integer ids to keep URLs lean.

# Schema Summary

* `Nfl::Player` stores basic information about an NFL player including name, position, etc. Has many week stats.
* `Nfl::WeekStat` stores general information about a week in NFL stats. May or may not have one of different subcategories of stats (receiving, rushing, etc.)
* `Nfl::KickingStat`, `Nfl::PassingStat`, `Nfl::ReceivingStat`, and `Nfl::RushingStat` all hold metadata about their particular stats (rush yards, receiving tds, interceptions, etc.) These belong to `Nfl::WeekStat`.

# Design Decisions

* Nfl namespace__: Provides a way to cleanly support other sports/leagues in the future without colliding model names or a messy file tree.
* __Generic Week Stat Model__: Instead of having the individual category stats belong directly to players, provide a sort-of intermediary model that can store other miscellaneous info about that week for that player. For example, if we wanted to add the number of flags a player got that week, we could add it here. Also allows the individual stat models to belong to say a `NFL::YearStat` model if we so chose to add one in the future.
* __Individual Stat Models__: Since kicking stats are completely unrelated to rushing stats and so forth, provide completely separate tables to store the data.
